﻿using MySample.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySample.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbFactory dbFactory;
        private SampleDbContext context;

        public UnitOfWork(IDbFactory dbFactory)
        {
            this.dbFactory = dbFactory;
        }

        public SampleDbContext SampleContext
        {
            get { return context ?? (dbFactory.Init()); }
        }
        public void Commit()
        {
            context.Commit();
        }
    }
}
