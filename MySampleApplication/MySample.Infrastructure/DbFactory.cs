﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySample.Data;

namespace MySample.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        SampleDbContext context;
        public SampleDbContext Init()
        {
            return context ?? (new SampleDbContext());
        }

        protected override void DisposeCore()
        {
            if (context != null)
                context.Dispose();
        }
    }
}
