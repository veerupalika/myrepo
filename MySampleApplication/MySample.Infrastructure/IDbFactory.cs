﻿using MySample.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySample.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        SampleDbContext Init();
    }
}
