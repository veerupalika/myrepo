﻿using MySample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySample.Infrastructure
{
    public class CategoryRepository : RepositoryBase<Category>, IRepository<Category>
    {
        public CategoryRepository(IDbFactory dbFactory) : base(dbFactory) { }

        public Category GetCategoryByName(string categoryName)
        {
            return this.DbContext.Categories.Where(c => c.Name.Equals(categoryName)).FirstOrDefault();
        }

        public override void Update(Category entity)
        {
            entity.DateUpdated = DateTime.Now;
            base.Update(entity);
        }
    }

    public interface ICategoryRepository : IRepository<Category>
    {
        Category GetCategoryByName(string categoryName);
    }
}
