﻿using MySample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySample.Infrastructure
{
    public class GadgetRepository : RepositoryBase<Gadget>, IRepository<Gadget>
    {
        public GadgetRepository(IDbFactory dbFactory) : base(dbFactory) { }

    }

    public interface IGadgetRepository : IRepository<Gadget>
    {

    }
}
